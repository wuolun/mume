# Mume-一款Typecho主题

> 由于工作室即将关闭，残废版可下载压缩包解压后体验，本仓库不再维护

## 介绍
一款Typecho简约主题，仅900KB左右，适合小型博客，支持 **HTML标签加持** （需要开启相应功能）

## 鸣谢
[MDUI](http://mdui.org)前端框架

[Fontawesome](https://fontawesome.com.cn)图标库

[GoogleFont](https://fonts.google.com)字体库

[极客族](?)字体库CDN


## 安装教程

1.  下载压缩包
2.  放入`usr/theme`目录中
3.  解压
> 要注意看主题解压后的文件夹名是 **Mume** 才能正常使用！


## 主题预览
在1.2（Developer）版本出来之前暂不展示，因为每天都在更新主题
可以前往[官方新闻中心](http://news.x-turbo.top)预览
## Wiki
使用文档在[Wiki](https://gitee.com/fuckerboy/mume/wikis/开始)里面，请前往查看
## 开发人员
X-turbo
.......